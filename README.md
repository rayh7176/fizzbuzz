# FIZZ BUZZ

The exercise can be solve with a simple JAVA 8 code below

IntStream.rangeClosed(1, 100)
    .mapToObj(i -> i % 3 == 0 ? (i % 5 == 0 ? "FizzBuzz" : "Fizz") : (i % 5 == 0 ? "Buzz" : i))
    .forEach(System.out::println);

In an event a small set for sure the solution above is favoured

Or We can solve it following the structure below

The below approach is a bit over engineered but the purpose of it is to show pattern design that can be used to avoid
breaking the Object design principals such Open Close Principle.

- Design pattern
  + Chain of Responsibilities is used to
    .   enable the application to expand as required in event a new type of calculation is added
    .   Triggers can be tested independently
    .   Triggers can be reassembled differently

  + StringBuilder was used to enhance the performance of capturing the data
      . Note: StringBuilder is regarded by some as Builder pattern and claimed by others to be more of a composite Pattern
              Pattern Builder: separate the construction of a complex object from its representation so that the same construction
              process can create different representations.
              StringBuilder: create step by step object rather than in one go. It does not meet a strict reading of the
              GoF definition of the design pattern but it facilitate the structure.

@Required.
- Java 8.
- mvn

In order to run the application you need to do the following

- mvn clean install


