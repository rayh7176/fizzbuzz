package com.fizzbuzz.calc;

import com.fizzbuzz.calc.impl.FizzBuzzImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class FizzBuzzTest {

    private FizzBuzz fizzBuzz;

    @BeforeEach
    public void before(){
        fizzBuzz = new FizzBuzzImpl();
    }

    @ParameterizedTest
    @MethodSource("nominatorDenominatorThree")
    public void testDivideNominatorByThree(int nominator, int denominator, boolean expectedResult){
        boolean statusResults = fizzBuzz.divideNominatorDenominator(nominator, denominator);
        assertThat(statusResults, is(expectedResult));
    }

    @ParameterizedTest
    @MethodSource("nominatorDenominatorFive")
    public void testDivideNominatorByFive(int nominator, int denominator, boolean expectedResult){
        boolean statusResults = fizzBuzz.divideNominatorDenominator(nominator, denominator);
        assertThat(statusResults, is(expectedResult));
    }

    public Stream<Arguments> nominatorDenominatorThree(){
        return Stream.of(Arguments.of(15, 3, Boolean.TRUE ),
                Arguments.of(15, 3, Boolean.TRUE ),
                Arguments.of(20, 3, Boolean.FALSE ),
                Arguments.of(27, 3, Boolean.TRUE ),
                Arguments.of(37, 3, Boolean.FALSE ),
                Arguments.of(66, 3, Boolean.TRUE ));
    }

    public Stream<Arguments> nominatorDenominatorFive(){
        return Stream.of(Arguments.of(15, 3, Boolean.TRUE ),
                Arguments.of(15, 5, Boolean.TRUE ),
                Arguments.of(20, 5, Boolean.TRUE ),
                Arguments.of(27, 5, Boolean.FALSE ),
                Arguments.of(37, 5, Boolean.FALSE ),
                Arguments.of(66, 5, Boolean.FALSE ));
    }

}
