package com.fizzbuzz.service;

import com.fizzbuzz.calc.FizzBuzz;
import com.fizzbuzz.calc.impl.FizzBuzzImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TriggerChainTest {
    private TriggerChain triggerChain;

    @BeforeEach
    public void before(){
        triggerChain = new TriggerChain(500);
    }

    @ParameterizedTest
    @MethodSource("calculateData")
    public void testCalculate(int number, int firstDeNominator, int secondDenominator, String expectedResult){
        triggerChain.registerChain(firstDeNominator, secondDenominator);
        String calculateResults = triggerChain.calculate(number);
        assertThat(calculateResults, is(expectedResult));
    }

    private Stream<Arguments> calculateData(){
        return Stream.of(Arguments.of(10, 3, 5, "1\n2\nFIZZ\n4\nBUZZ\nFIZZ\n7\n8\nFIZZ\nBUZZ\n"),
                         Arguments.of(20, 3, 5, "1\n2\nFIZZ\n4\nBUZZ\nFIZZ\n7\n8\nFIZZ\nBUZZ\n11\nFIZZ\n13\n14\nFIZZBUZZ\n16\n17\nFIZZ\n19\nBUZZ\n"),
                         Arguments.of(30, 3, 5, "1\n2\nFIZZ\n4\nBUZZ\nFIZZ\n7\n8\nFIZZ\nBUZZ\n11\nFIZZ\n13\n14\nFIZZBUZZ\n16\n17\nFIZZ\n19\nBUZZ\nFIZZ\n22\n23\nFIZZ\nBUZZ\n26\nFIZZ\n28\n29\nFIZZBUZZ\n"),
                         Arguments.of(40, 3, 5, "1\n2\nFIZZ\n4\nBUZZ\nFIZZ\n7\n8\nFIZZ\nBUZZ\n11\nFIZZ\n13\n14\nFIZZBUZZ\n16\n17\nFIZZ\n19\nBUZZ\nFIZZ\n22\n23\nFIZZ\nBUZZ\n26\nFIZZ\n28\n29\nFIZZBUZZ\n31\n32\nFIZZ\n34\nBUZZ\nFIZZ\n37\n38\nFIZZ\nBUZZ\n"),
                         Arguments.of(50, 3, 5, "1\n2\nFIZZ\n4\nBUZZ\nFIZZ\n7\n8\nFIZZ\nBUZZ\n11\nFIZZ\n13\n14\nFIZZBUZZ\n16\n17\nFIZZ\n19\nBUZZ\nFIZZ\n22\n23\nFIZZ\nBUZZ\n26\nFIZZ\n28\n29\nFIZZBUZZ\n31\n32\nFIZZ\n34\nBUZZ\nFIZZ\n37\n38\nFIZZ\nBUZZ\n41\nFIZZ\n43\n44\nFIZZBUZZ\n46\n47\nFIZZ\n49\nBUZZ\n"),
                         Arguments.of(60, 3, 5, "1\n2\nFIZZ\n4\nBUZZ\nFIZZ\n7\n8\nFIZZ\nBUZZ\n11\nFIZZ\n13\n14\nFIZZBUZZ\n16\n17\nFIZZ\n19\nBUZZ\nFIZZ\n22\n23\nFIZZ\nBUZZ\n26\nFIZZ\n28\n29\nFIZZBUZZ\n31\n32\nFIZZ\n34\nBUZZ\nFIZZ\n37\n38\nFIZZ\nBUZZ\n41\nFIZZ\n43\n44\nFIZZBUZZ\n46\n47\nFIZZ\n49\nBUZZ\nFIZZ\n52\n53\nFIZZ\nBUZZ\n56\nFIZZ\n58\n59\nFIZZBUZZ\n"),
                         Arguments.of(70, 3, 5, "1\n2\nFIZZ\n4\nBUZZ\nFIZZ\n7\n8\nFIZZ\nBUZZ\n11\nFIZZ\n13\n14\nFIZZBUZZ\n16\n17\nFIZZ\n19\nBUZZ\nFIZZ\n22\n23\nFIZZ\nBUZZ\n26\nFIZZ\n28\n29\nFIZZBUZZ\n31\n32\nFIZZ\n34\nBUZZ\nFIZZ\n37\n38\nFIZZ\nBUZZ\n41\nFIZZ\n43\n44\nFIZZBUZZ\n46\n47\nFIZZ\n49\nBUZZ\nFIZZ\n52\n53\nFIZZ\nBUZZ\n56\nFIZZ\n58\n59\nFIZZBUZZ\n61\n62\nFIZZ\n64\nBUZZ\nFIZZ\n67\n68\nFIZZ\nBUZZ\n"),
                         Arguments.of(80, 3, 5, "1\n2\nFIZZ\n4\nBUZZ\nFIZZ\n7\n8\nFIZZ\nBUZZ\n11\nFIZZ\n13\n14\nFIZZBUZZ\n16\n17\nFIZZ\n19\nBUZZ\nFIZZ\n22\n23\nFIZZ\nBUZZ\n26\nFIZZ\n28\n29\nFIZZBUZZ\n31\n32\nFIZZ\n34\nBUZZ\nFIZZ\n37\n38\nFIZZ\nBUZZ\n41\nFIZZ\n43\n44\nFIZZBUZZ\n46\n47\nFIZZ\n49\nBUZZ\nFIZZ\n52\n53\nFIZZ\nBUZZ\n56\nFIZZ\n58\n59\nFIZZBUZZ\n61\n62\nFIZZ\n64\nBUZZ\nFIZZ\n67\n68\nFIZZ\nBUZZ\n71\nFIZZ\n73\n74\nFIZZBUZZ\n76\n77\nFIZZ\n79\nBUZZ\n"),
                         Arguments.of(90, 3, 5, "1\n2\nFIZZ\n4\nBUZZ\nFIZZ\n7\n8\nFIZZ\nBUZZ\n11\nFIZZ\n13\n14\nFIZZBUZZ\n16\n17\nFIZZ\n19\nBUZZ\nFIZZ\n22\n23\nFIZZ\nBUZZ\n26\nFIZZ\n28\n29\nFIZZBUZZ\n31\n32\nFIZZ\n34\nBUZZ\nFIZZ\n37\n38\nFIZZ\nBUZZ\n41\nFIZZ\n43\n44\nFIZZBUZZ\n46\n47\nFIZZ\n49\nBUZZ\nFIZZ\n52\n53\nFIZZ\nBUZZ\n56\nFIZZ\n58\n59\nFIZZBUZZ\n61\n62\nFIZZ\n64\nBUZZ\nFIZZ\n67\n68\nFIZZ\nBUZZ\n71\nFIZZ\n73\n74\nFIZZBUZZ\n76\n77\nFIZZ\n79\nBUZZ\nFIZZ\n82\n83\nFIZZ\nBUZZ\n86\nFIZZ\n88\n89\nFIZZBUZZ\n"),
                         Arguments.of(100, 3, 5, "1\n2\nFIZZ\n4\nBUZZ\nFIZZ\n7\n8\nFIZZ\nBUZZ\n11\nFIZZ\n13\n14\nFIZZBUZZ\n16\n17\nFIZZ\n19\nBUZZ\nFIZZ\n22\n23\nFIZZ\nBUZZ\n26\nFIZZ\n28\n29\nFIZZBUZZ\n31\n32\nFIZZ\n34\nBUZZ\nFIZZ\n37\n38\nFIZZ\nBUZZ\n41\nFIZZ\n43\n44\nFIZZBUZZ\n46\n47\nFIZZ\n49\nBUZZ\nFIZZ\n52\n53\nFIZZ\nBUZZ\n56\nFIZZ\n58\n59\nFIZZBUZZ\n61\n62\nFIZZ\n64\nBUZZ\nFIZZ\n67\n68\nFIZZ\nBUZZ\n71\nFIZZ\n73\n74\nFIZZBUZZ\n76\n77\nFIZZ\n79\nBUZZ\nFIZZ\n82\n83\nFIZZ\nBUZZ\n86\nFIZZ\n88\n89\nFIZZBUZZ\n91\n92\nFIZZ\n94\nBUZZ\nFIZZ\n97\n98\nFIZZ\nBUZZ\n"));
    }
}
