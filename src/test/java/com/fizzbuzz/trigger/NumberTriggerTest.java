package com.fizzbuzz.trigger;

import com.fizzbuzz.calc.FizzBuzz;
import com.fizzbuzz.calc.impl.FizzBuzzImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class NumberTriggerTest {
    private NumberTrigger numberTrigger;

    @BeforeEach
    public void before(){
        numberTrigger = new NumberTrigger(0);
    }

    @ParameterizedTest
    @MethodSource("nominatorValue")
    public void testIsTriggeredByNone(int nominator, String expectedResult){
        String valueResults = numberTrigger.isTriggeredBy(nominator);
        assertThat(valueResults, is(expectedResult));
    }

    public Stream<Arguments> nominatorValue(){
        return Stream.of(Arguments.of(3, "3" ),
                Arguments.of(5, "5" ),
                Arguments.of(15, "15" ),
                Arguments.of(37, "37" ),
                Arguments.of(66, "66" ));
    }
}
