package com.fizzbuzz.trigger;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class FizzBuzzTriggerTest {
    private FizzBuzzTrigger fizzBuzzTrigger;

    @BeforeEach
    public void before(){
        fizzBuzzTrigger = new FizzBuzzTrigger(15);

    }

    @ParameterizedTest
    @MethodSource("nominatorValue")
    public void testIsTriggeredByFifteen(int nominator, String expectedResult){
        fizzBuzzTrigger.registerNext(new NumberTrigger(0));
        String valueResults = fizzBuzzTrigger.isTriggeredBy(nominator);
        assertThat(valueResults, is(expectedResult));
    }

    public Stream<Arguments> nominatorValue(){
        return Stream.of(Arguments.of(3, "3" ),
                Arguments.of(5, "5" ),
                Arguments.of(15, "FIZZBUZZ" ),
                Arguments.of(20, "20" ),
                Arguments.of(30, "FIZZBUZZ" ),
                Arguments.of(66, "66" ));
    }
}
