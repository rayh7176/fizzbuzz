package com.fizzbuzz.trigger;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class FizzTriggerTest {
    private FizzTrigger fizzTrigger;

    @BeforeEach
    public void before(){
        fizzTrigger = new FizzTrigger(3);

    }

    @ParameterizedTest
    @MethodSource("nominatorValue")
    public void testIsTriggeredByThree(int nominator, String expectedResult){
        fizzTrigger.registerNext(new NumberTrigger(0));
        String valueResults = fizzTrigger.isTriggeredBy(nominator);
        assertThat(valueResults, is(expectedResult));
    }

    @ParameterizedTest
    @MethodSource("nominatorValue")
    public void testIsTriggeredByThree_withNullNextTrigger(int nominator, String expectedResult){
        String valueResults = fizzTrigger.isTriggeredBy(nominator);
        assertThat(valueResults, is(expectedResult));
    }

    public Stream<Arguments> nominatorValue(){
        return Stream.of(Arguments.of(3, "FIZZ" ),
                Arguments.of(5, "5" ),
                Arguments.of(15, "FIZZ" ),
                Arguments.of(20, "20" ),
                Arguments.of(37, "37" ),
                Arguments.of(66, "FIZZ" ));
    }
}
