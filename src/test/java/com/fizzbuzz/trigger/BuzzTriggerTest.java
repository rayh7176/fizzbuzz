package com.fizzbuzz.trigger;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class BuzzTriggerTest {
    private BuzzTrigger buzzTrigger;

    @BeforeEach
    public void before(){
        buzzTrigger = new BuzzTrigger(5);
    }

    @ParameterizedTest
    @MethodSource("nominatorValue")
    public void testIsTriggeredByFive(int nominator, String expectedResult){
        buzzTrigger.registerNext(new NumberTrigger(0));
        String valueResults = buzzTrigger.isTriggeredBy(nominator);
        assertThat(valueResults, is(expectedResult));
    }

    @ParameterizedTest
    @MethodSource("nominatorValue")
    public void testIsTriggeredByFive_withNullNextTrigger(int nominator, String expectedResult){
        String valueResults = buzzTrigger.isTriggeredBy(nominator);
        assertThat(valueResults, is(expectedResult));
    }

    public Stream<Arguments> nominatorValue(){
        return Stream.of(Arguments.of(3, "3" ),
                Arguments.of(15, "BUZZ" ),
                Arguments.of(20, "BUZZ" ),
                Arguments.of(37, "37" ),
                Arguments.of(66, "66" ));
    }
}
