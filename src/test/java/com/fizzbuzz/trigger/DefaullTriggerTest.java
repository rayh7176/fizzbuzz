package com.fizzbuzz.trigger;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInstance;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class DefaullTriggerTest {
    private static DefaultTrigger defaultTrigger;

    @BeforeClass
    static public void before(){
        defaultTrigger = new DefaultTrigger(5) {
            @Override
            public String isTriggeredBy(int number) {
                return "TRUE";
            }
        };
    }

    @Test
    public void testRegisterNext(){
        DefaultTrigger expectedTrigger = getInstanceTrigger();
        DefaultTrigger receivedTrigger = defaultTrigger.registerNext(expectedTrigger);
        assertThat(receivedTrigger, is(expectedTrigger));
        DefaultTrigger receivedNextTrigger = defaultTrigger.getNextTrigger();
        assertThat(receivedNextTrigger, is(expectedTrigger));
    }

    @Test
    public void testDenominator(){
        int expectedDenominator = 5;
        int receivedDenominator = defaultTrigger.getDenominator();
        assertThat(receivedDenominator, is(expectedDenominator));
    }

    @Test
    public void testDefaultValue(){
        String expectedValue = "200";
        String receivedValue = defaultTrigger.getDefaultValueNumber(200);
        assertThat(receivedValue, is(expectedValue));
    }

    private DefaultTrigger getInstanceTrigger(){
        return new DefaultTrigger(3) {
            @Override
            public String isTriggeredBy(int number) {
                return "FALSE";
            }
        };
    }
}
