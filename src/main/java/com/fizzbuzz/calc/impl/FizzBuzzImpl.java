package com.fizzbuzz.calc.impl;

import com.fizzbuzz.calc.FizzBuzz;

public class FizzBuzzImpl implements FizzBuzz {

    @Override
    public boolean divideNominatorDenominator(int numerator, int denominator) {
        return numerator % denominator == 0;
    }
}
