package com.fizzbuzz.calc;

public interface FizzBuzz {
    boolean divideNominatorDenominator(int numerator, int denominator);
}
